
package ir_system.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Любая фаза информационного поиска.
 */
abstract public class IR_Phase {
    
    // Коллекция артефактов, порожденных к этому моменту 
    protected ArtifactCollection artifactCollection; 
    
    public IR_Phase(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        phaseList.add(this); // при создании фазы она автоматически помещается в список
        this.artifactCollection = artifactCollection;
    }
    
    // ---------------- Статус исполнения фазы ---------------------
    private IR_PhaseStatus status = IR_PhaseStatus.OK;
    
    public IR_PhaseStatus getStatus() {
        return status;
    }
    
    // ----------------------- Учет ошибок --------------------------
    protected String error = "";
     
    public String getError() {
        return error;
    }

    protected void throwError(String errorDescription) throws Exception {
        status = IR_PhaseStatus.ERROR; 
        error = errorDescription;
        throw new Exception(error); 
    }

    
    // -------------------- Учет предупреждений -----------------------
    protected List<String> warningList = new ArrayList<>();

    public List<String> getWarningList() {
        return Collections.unmodifiableList(warningList);
    }
    
    protected void addWarning(String warningDescription) {
        assert status != IR_PhaseStatus.ERROR;
        
        status = IR_PhaseStatus.WARNING; 
        warningList.add(warningDescription);
    }
   
    // -----------------------------------------------------------------
    
    /**
     * Запуск фазы информационного поиска
     */
    public void run() throws Exception {
        checkPreconditions();   // проверка доступных артефактов
        perform();              // исполнение фазы
    } 

// -------------------------- TO DO -----------------------------------    

    /**
     * Уникальный идентификатор фазы
     */
    abstract public String getId();
    
    /**
     * Проверка возможности запуска фазы. Обычно проверяется наличие 
     * необходимых артефактов
     */
    abstract public void checkPreconditions() throws Exception; 
    
    /**
     * Исполнение фазы информационного поиска
     */
    abstract public void perform() throws Exception; 
}
