
package ir_system.model;

import ir_system.model.phase.DocumentCollectionSnapshot;
import ir_system.model.phase.Tokenization;
import ir_system.model.phase.Indexing;
import ir_system.model.phase.QueryParsing;
import ir_system.model.phase.QuerySnapshot;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Модель процесса информационного поиска
 */
public class IR_Model {
    
    // Пути к директориям с коллекциям документов и порождаемых артефактов
    public static final String DOCUMENT_COLLECTION_DIRECTORY_NAME = "/_DocumentCollection";
    public static final String ARTIFACT_COLLECTION_DIRECTORY_NAME = "/_Artifacts";
    
    /**
     * Путь к текущей директории программы
     */
    public static String getAbsolutePath() {
       return new File("").getAbsolutePath();
    }
    
    /**
     * Список всех прошедших фаз информационного поиска
     */
    private final List<IR_Phase> phaseList = new ArrayList<>();
    /**
     * Коллекция всех артефактов, созданных в процессе информационного поиска
     */
    private final ArtifactCollection artifactCollection = new ArtifactCollection(); 
    
    
    /* ------------- Публикация ошибок и предупреждений, -------------
       ------------- полученных в процессе информационного поиска ----------- */
    private static final Logger log = Logger.getLogger(IR_Model.class.getName());
    
    private void publishWarningList() {
        for(IR_Phase phase : phaseList) // для каждой фазы
        {
            for(String warning : phase.getWarningList()) // для каждого предупркеждения
            {
                log.info("Warning >> " + phase.getId() + ": " + warning);
            }
        }
    }
    
    private void publishErrorList() {
        for(IR_Phase phase : phaseList) // для каждой фазы
        {
            if(phase.getStatus() == IR_PhaseStatus.ERROR)   // если фаза завершилась ошибкой
            {
                log.info("ERROR >> " + phase.getId() + ": " + phase.getError());
            }
        }
    }
    
   // --------- TO DO: Расширяемый процесс информационного поиска ------------- 
    public void run()  {
        
        try {
            // Построение списка документов из коллекции документов
            new DocumentCollectionSnapshot(phaseList, artifactCollection).run();
            
            // Разделение документов на лексемы
            new Tokenization(phaseList, artifactCollection).run();
            
            // Построение инвертированного индекса
            new Indexing(phaseList, artifactCollection).run();
            
            // Получение запроса
            new QuerySnapshot(phaseList, artifactCollection).run();
            
            // Разбор запроса
            new QueryParsing(phaseList, artifactCollection).run();
            
            // Публикация полученных предупреждений
            publishWarningList();           
            
       } catch (Exception ex) {
           
            // Публикация полученных ошибок
            log.log(Level.SEVERE, null, ex);
            publishErrorList();
        }
    }
}
