
package ir_system.model;

/**
 * Возможные исходы фазы информационного поиска.
 */
public enum IR_PhaseStatus {
    ERROR,
    WARNING,
    OK
}
