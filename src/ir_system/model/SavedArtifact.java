
package ir_system.model;

/**
 * Артефакт, содержимое которого сохраняется в файл для отслеживания 
 * промежуточных результатов информационного поиска.
*/
abstract public class SavedArtifact extends Artifact {

    /**
     * Путь к файлу, в который сохраняется содержимое артефакта
     */
    protected String getPath() {
        return IR_Model.getAbsolutePath() + IR_Model.ARTIFACT_COLLECTION_DIRECTORY_NAME + "/" + getId() + ".txt";
    }
    
// -------------------------- TO DO -----------------------------------     
    
    /**
     * Сохранение содержимого артефака в файл
     */
    abstract public void save() throws Exception;
}
