
package ir_system.model.artifact;

import ir_system.model.SavedArtifact;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javafx.util.Pair;

/**
 * Запрос
 */
public class Query extends SavedArtifact {

    @Override
    public String getId() {
        return "Query";
    }

    
    private String value;
    
    // ---------------------- Чтение-запись ---------------------------
    
    public void setValue(String query) {
        value = query;
    }
    
    public String getValue() {
        return value;
    }
    
    // ---------------------------------------------------------------
    /**
     * Сохраняет в файл содержимое запроса
     */
    @Override
    public void save() throws Exception {
        try( BufferedWriter outStream = new BufferedWriter(new FileWriter(getPath())) ) // открываем выходной поток
        {
           outStream.write(value);
        }
        catch(IOException ex)
        {
            throw ex;   // транслируем исключение далее
        }
    }
}
