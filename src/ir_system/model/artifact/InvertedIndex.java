
package ir_system.model.artifact;

import ir_system.model.SavedArtifact;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class InvertedIndex extends SavedArtifact {

    @Override
    public String getId() {
        return "InvertedIndex";
    }

    // Словарь терминов и словопозиций 
    private final Map<String, Set<Integer>> invertedIndex = new TreeMap<>();
    
    
    // ----------------- Чтение-запись терминов ----------------- 
    
    public void add(String term, int docId) {
        
        // Создаем список словопозииций для нового термина
        if(!invertedIndex.containsKey(term))
        {
            Set<Integer> put = invertedIndex.put(term, new TreeSet<>());
        }    
        
        // Добавляем словопозицию
        invertedIndex.get(term).add(docId);
    }
    
    // ---------------------------------------------------------------
    /**
     * Сохраняет в файл термины и их словопозиции
     */
    @Override
    public void save() throws Exception {
        
        try( BufferedWriter outStream = new BufferedWriter(new FileWriter(getPath())) ) // открываем выходной поток
        {
            for(String term : invertedIndex.keySet())  // для каждого термина 
            {
                // Преобразуем к читаемому виду и сохраняем
                outStream.write(term + " [" + invertedIndex.get(term).size() + "]\t");
                for(Integer docId : invertedIndex.get(term))    // для каждой словопозиции
                {               
                    outStream.write(Document.idTotString(docId) + " ");
                }
                outStream.newLine();
            }
        }
        catch(IOException ex)
        {
            throw ex;   // транслируем исключение далее
        }
    }
}
