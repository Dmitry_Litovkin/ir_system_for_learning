
package ir_system.model.artifact;

import ir_system.model.SavedArtifact;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Особый артефакт, который представляет собой исходную коллекцию документов и 
 * порождаемый список-словарь документов
 */
public class DocumentCollection extends SavedArtifact implements Iterable<Document> {

    @Override
    public String getId() {
        return "DocumentList";
    }

    // Словарь документов
    private final Map<Integer, Document> map = new HashMap<>();        
    

    // --- Генерация уникальных идентификаторов для каждого документа ----
    private static int currentDocId = 0;

    static int generateDocumentId() {
        return ++currentDocId;
    }
    
    // ----------------- Чтение-запись документов -----------------
    
    public int size() {
        return map.size();
    }
    
    public Document get(int docId) {
        return map.get(docId);
    }
    
    public void put(Document doc) {
        map.put(doc.getId(), doc);        
    }

    @Override
    public Iterator<Document> iterator() {
        return Collections.unmodifiableCollection(map.values()).iterator();
    }
    
    // ---------------------------------------------------------------
    /**
     * Сохраняет в файл список-словарь используемых документов 
     */
    @Override
    public void save() throws Exception {
        
        try( BufferedWriter outStream = new BufferedWriter(new FileWriter(getPath())) ) // открываем выходной поток
        {
            for(Document doc : map.values()) // для каждого документа
            {
                // Преобразуем к читаемому виду и сохраняем
                outStream.write(Document.idTotString(doc.getId()) + "\t" + doc.getFilename());
                outStream.newLine();
            }
        }
        catch(IOException ex)
        {
            throw ex;   // транслируем исключение далее
        }
    }    
}
