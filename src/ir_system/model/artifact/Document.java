
package ir_system.model.artifact;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Formatter;

/**
 * Документ из коллекции
 */
public class Document {

    // --------------- Уникальный идентификатор документа ---------------------
    private final int id;
    
    public int getId() {
        return id;
    }
    
    static public String idTotString(int docId) {
        
        StringBuilder sb = new StringBuilder();
        Formatter idFormatter = new Formatter(sb);
        idFormatter.format("%03d", docId);
        
        return sb.toString();
    }
    
    // --------------- Путь к файлу документа ---------------------
    
    private final String path;
   
    public String gePath() {
        return path;
    }
    
    public String getFilename() {
        return path.substring(path.lastIndexOf('\\')+1);
    }

    // -------------------------------------------------------------

    public Document(String path) {
        id = DocumentCollection.generateDocumentId();
        this.path = path;
    }    
  
    // -------------- Чтение содержимого документа -------------------
    
    private BufferedReader inputStream;

    public void open() throws Exception {
        inputStream = new BufferedReader(new FileReader(path));
    }
    
    public String readLine() throws IOException {
        return inputStream.readLine();
    }   
    
    public void close() throws IOException {
       inputStream.close();
    }
}
