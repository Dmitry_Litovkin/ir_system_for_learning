
package ir_system.model.artifact;

import ir_system.model.SavedArtifact;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javafx.util.Pair;

/**
 * Список лексем всех документов
 */
public class TokenList extends SavedArtifact implements Iterable<Pair<String, Integer>> {

    @Override
    public String getId() {
        return "TokenList";
    }
    
    // Список пар лексема-идентификатор_документа 
    private final List<Pair<String, Integer>> tokenList = new ArrayList<>();
    
    // ----------------- Чтение-запись лексем ----------------------- 
    public int size(){
        return tokenList.size();
    }
    
    public void add(String token, int docId) {
        tokenList.add(new Pair(token, docId));
    }
    
    @Override
    public Iterator<Pair<String, Integer>> iterator() {
        return Collections.unmodifiableCollection(tokenList).iterator();
    }

    // ---------------------------------------------------------------
    /**
     * Сохраняет в файл список пар лексема-идентификатор_документа
     */
    @Override
    public void save() throws Exception {
        
        try( BufferedWriter outStream = new BufferedWriter(new FileWriter(getPath())) ) // открываем выходной поток
        {
            for(Pair<String, Integer> token_docId : tokenList) // для каждой лексемы
            {
                // Преобразуем к читаемому виду и сохраняем
                outStream.write(token_docId.getKey() + " ");
                outStream.write(Document.idTotString(token_docId.getValue()));
                outStream.newLine();
            }
        }
        catch(IOException ex)
        {
            throw ex;   // транслируем исключение далее
        }
    }
}
