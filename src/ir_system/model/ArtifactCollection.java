
package ir_system.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Коллекция всех артефактов, порождаемых в процессе информационного поиска.
 */
public class ArtifactCollection  implements Iterable<Artifact> {
    
    // Словарь артефактов
    private final Map<String, Artifact> map = new HashMap<>();        
    
    // -----------------  Чтение-запись артефактов -----------------
    public Artifact get(String artifactId) {
        return map.get(artifactId);
    }

    public void put(Artifact artifact) {
        map.put(artifact.getId(), artifact);
    }

    public int size() {
        return map.size();
    }
    
    @Override
    public Iterator<Artifact> iterator() {
        return Collections.unmodifiableCollection(map.values()).iterator();
    }
}
