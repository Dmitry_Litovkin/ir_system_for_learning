
package ir_system.model.phase;

import ir_system.model.ArtifactCollection;
import ir_system.model.IR_Phase;
import ir_system.model.artifact.ParseTreeOfQuery;
import ir_system.model.artifact.Query;
import java.util.List;

/**
 * Разбор запроса
 */
public class QueryParsing extends IR_Phase  {

    public QueryParsing(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        super(phaseList, artifactCollection);
    }

    @Override
    public String getId() {
        return "QueryParsing";
    }

    @Override
    public void checkPreconditions() throws Exception {
        // Требуется запрос
        if(artifactCollection.get("Query") == null)
        {
            throwError("Query is absent");
        }
    }

    @Override
    public void perform() throws Exception {
        
        // Используемый артефакт - запрос
        Query query = (Query) this.artifactCollection.get("Query");

        // Создаваемый артефакт - дерево запроса 
        ParseTreeOfQuery parseTreOfQuery= new ParseTreeOfQuery();
        
        // TO DO TO DO TO DO TO DO TO DO TO DO
        
        // Сохраняем полученный артефакт
        try 
        {
            parseTreOfQuery.save();
            artifactCollection.put(parseTreOfQuery);
        }
        catch(Exception ex)
        {
            throwError("Can not save parse tree of query");
        }        
    }    
}
