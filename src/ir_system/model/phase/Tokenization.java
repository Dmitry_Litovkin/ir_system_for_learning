
package ir_system.model.phase;

import ir_system.model.ArtifactCollection;
import ir_system.model.IR_Phase;
import ir_system.model.artifact.Document;
import ir_system.model.artifact.DocumentCollection;
import ir_system.model.artifact.TokenList;
import ir_system.model.phase.lexer.Lexer;
import ir_system.model.phase.lexer.PrimitiveLexer;
import java.util.List;

/**
 * Разделение документов на лексемы
 */
public class Tokenization extends IR_Phase {

    @Override
    public String getId() {
        return "Tokenization";
    }

    public Tokenization(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        super(phaseList, artifactCollection);
    }

    @Override
    public void checkPreconditions() throws Exception {
        // Требуется список-словарь документов
        if(artifactCollection.get("DocumentList") == null)
        {
            throwError("Document list is absent");
        }
    }

    /**
     * Разделяет документы на лексемы и создает общий список лексем
     */
    @Override
    public void perform() throws Exception {
        // Используемый артефакт - список-словарь документов
        DocumentCollection docCollection = (DocumentCollection) artifactCollection.get("DocumentList");
        
        // Создаваемый артефакт - список пар лексема-идентификатор_документа
        TokenList tokenList = new TokenList();
        
        boolean isFileWrong = false;    // файл не считывается
        
        // Используемый лексический анализатор
        Lexer lexer = new PrimitiveLexer();
            
        try
        {
            // Выделяем построчно из документов лексемы и записываем их в список
            for(Document doc : docCollection) // для каждого документа
            {
                doc.open();

                String line = doc.readLine();
                while(line != null) // пока документ не закончился
                {
                    // Разделяем строку на лексемы
                    String[] tokens = lexer.scan(line);

                    for(String token : tokens)  // для каждой выделенной лексемы
                    {
                        tokenList.add(token, doc.getId());
                    }
                    line = doc.readLine();
                }

                doc.close();
            }
        }
        catch(Exception ex)
        {
            isFileWrong = true;           
            throwError("Document file is wrong");
        }        
        
        // Сохраняем полученный артефакт
        if(!isFileWrong)
        {
            try 
            {
                tokenList.save();
                artifactCollection.put(tokenList);
            }
            catch(Exception ex)
            {
                throwError("Can not save token list");
            }        
        }
    }
}
