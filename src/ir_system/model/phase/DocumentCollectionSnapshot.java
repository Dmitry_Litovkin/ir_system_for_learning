
package ir_system.model.phase;

import ir_system.model.ArtifactCollection;
import ir_system.model.IR_Phase;
import ir_system.model.IR_Model;
import ir_system.model.artifact.Document;
import ir_system.model.artifact.DocumentCollection;
import java.io.File;
import java.util.List;

/**
 * Построение списка-словаря документов на основе коллекции документов.
 */
public class DocumentCollectionSnapshot extends IR_Phase {

    @Override
    public String getId() {
        return "DocumentCollectionSnapshot";
    }

    public DocumentCollectionSnapshot(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        super(phaseList, artifactCollection);
    }

    @Override
    public void checkPreconditions() throws Exception {
        // Артефакты не требуются
    }
    
    // ---------------------------------------------------------------------
    /**
     * Создает список-словарь документов, хранящихся в специальной директории
     */
    @Override
    public void perform() throws Exception {
        // Создаваемый артефакт - список-словарь документов
        DocumentCollection docCollection = new DocumentCollection();
        
        // Открываем директорию с документами
        File docCollectionDirectory = new File(IR_Model.getAbsolutePath() + IR_Model.DOCUMENT_COLLECTION_DIRECTORY_NAME);
        if(!docCollectionDirectory.isDirectory()) 
        {   throwError("Invalid directory for document's collection");   }

        // Каждому документу присваиваем идентификатор и запоминаем путь к его файлу
        for(File docFile : docCollectionDirectory.listFiles())
        {
            if(docFile.isDirectory()) 
            {   
                addWarning("Directory belongs to document's collection");  
            }
            else
            {
                docCollection.put( new Document(docFile.getPath()) );
            }
        }
        
        // Проверяем корректность полученного артефакта
        if(docCollection.size() == 0)
        { addWarning("Document's collection is empty"); }
        
        // Сохраняем полученный артефакт
        try 
        {
            docCollection.save();
            artifactCollection.put(docCollection);
        }
        catch(Exception ex)
        {
            throwError("Can not save document list");
        }    
    }
}
