package ir_system.model.phase.lexer;

/**
 * Произвольный лексический анализатор
 */
abstract public class Lexer {
    
    /**
     * Лексический анализ строки
     */
    abstract public String[] scan(String line);
}
