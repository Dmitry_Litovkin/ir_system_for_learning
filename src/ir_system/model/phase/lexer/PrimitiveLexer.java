
package ir_system.model.phase.lexer;

import ir_system.model.phase.lexer.Lexer;

/**
 * Примитивный лексический анализатор
 */
public class PrimitiveLexer extends Lexer {

    /**
     * Разделяет строку на лексемы, исключая некоторые разделители и 
     * последовательности пробелов
     */
    @Override
    public String[] scan(String line) {
        return line.split("\\s*(\\s|,|!|'|:|\\.)\\s*");
    }
}
