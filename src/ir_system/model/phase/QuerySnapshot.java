
package ir_system.model.phase;

import ir_system.model.ArtifactCollection;
import ir_system.model.IR_Phase;
import ir_system.model.artifact.Query;
import ir_system.model.artifact.TokenList;
import java.util.List;


/**
 * Получение запроса
 */
public class QuerySnapshot extends IR_Phase {

    public QuerySnapshot(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        super(phaseList, artifactCollection);
    }

    @Override
    public String getId() {
        return "QuerySnapshot";
    }

    @Override
    public void checkPreconditions() throws Exception {
        // Артефакты не требуются
    }

    @Override
    public void perform() throws Exception {

        // Создаваемый артефакт - запрос
        Query query = new Query();

        // Получаем запрос
        query.setValue("simple query");
        
        // Сохраняем полученный артефакт
        try 
        {
            query.save();
            artifactCollection.put(query);
        }
        catch(Exception ex)
        {
            throwError("Can not save query");
        }        
    }
}
