
package ir_system.model.phase;

import ir_system.model.ArtifactCollection;
import ir_system.model.IR_Phase;
import ir_system.model.artifact.InvertedIndex;
import ir_system.model.artifact.TokenList;
import java.util.List;
import javafx.util.Pair;

/**
 * Построение инвертированного индекса
 */
public class Indexing extends IR_Phase {

    @Override
    public String getId() {
        return "Indexing";
    }

    public Indexing(List<IR_Phase> phaseList, ArtifactCollection artifactCollection) {
        super(phaseList, artifactCollection);
    }

    @Override
    public void checkPreconditions() throws Exception {
        // Требуется список лексем
        if(artifactCollection.get("TokenList") == null)
        {
            throwError("Token list is absent");
        }
    }

    /**
    * На основе пар лексема-идентификатор_документа создает инвертированный индекс
    */
    @Override
    public void perform() throws Exception {
        // Используемый артефакт - список лексем
        TokenList tokenList = (TokenList) this.artifactCollection.get("TokenList");
        
        // Создаваемый артефакт - инвертированный индекс
        InvertedIndex index= new InvertedIndex();
        
        for(Pair<String, Integer> tokenAndDocId : tokenList) // для каждой пары лексем
        {
            index.add(tokenAndDocId.getKey(), tokenAndDocId.getValue());
        }

        // Сохраняем полученный артефакт
        try 
        {
            index.save();
            artifactCollection.put(index);
        }
        catch(Exception ex)
        {
            throwError("Can not save inverted index");
        }        
    }
}
